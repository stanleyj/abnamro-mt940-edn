(defproject mt940-edn "0.2.1-SNAPSHOT"
  :description "Parse MT940 statement files, export as EDN"
  :dependencies [[org.clojure/clojure "1.10.1"]]
  :main mt940-edn.core
  :aot [mt940-edn.core])
