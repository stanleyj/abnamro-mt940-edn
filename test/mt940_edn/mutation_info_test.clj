(ns mt940-edn.mutation-info-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [mt940-edn.util :refer [local-date]]
   [mt940-edn.mutation-info :as sut]))

(deftest copy-year-test
  (testing "Copy year"
    (is (= (sut/copy-year (local-date 2016 5 7) (local-date 2017 5 9) inc)
           (local-date 2018 5 7)))
    (is (= (sut/copy-year (local-date 2016 5 9) (local-date 2017 5 7) dec)
           (local-date 2016 5 9)))
    (is (= (sut/copy-year (local-date 2016 5 9) (local-date 2019 5 9) identity)
           (local-date 2019 5 9)))))

(deftest entry-date-with-year-test
  (testing "Entry date with year"
    (is (= (sut/entry-date-with-year
            (local-date 2016 5 7)
            (local-date 0 5 7))
           (local-date 2016 5 7)))
    (is (= (sut/entry-date-with-year
            (local-date 2016 12 31)
            (local-date 0 1 1))
           (local-date 2017 1 1)))
    (is (= (sut/entry-date-with-year
            (local-date 2017 1 1)
            (local-date 0 12 16))
           (local-date 2016 12 16)))
    (is (= (sut/entry-date-with-year
            (local-date 2017 1 1)
            (local-date 0 7 1))
           (local-date 2017 7 1)))
    (is (= (sut/entry-date-with-year
            (local-date 2017 1 1)
            (local-date 0 6 1))
           (local-date 2017 6 1)))))
