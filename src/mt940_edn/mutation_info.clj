(ns mt940-edn.mutation-info
  (:require
   [clojure.string :as str]
   [mt940-edn.util :as u]))

(defn month [d]
  (.getMonthValue d))

(defn copy-year
  "Copies year from  year-date to orig-date, and applies year-fn on year.
  Does not work if orig-date is February 29th and year to be copied is not a
  leap year after applying year-fn"
  [orig-date year-date year-fn]
  (u/local-date
   (year-fn (.getYear year-date)) (month orig-date) (.getDayOfMonth orig-date)))

(defn entry-date-with-year
  "Given a value-date with year and an entry date with year 0,
  return the entry date with correct year"
  [value-date entry-date]
  (copy-year
   entry-date value-date
   (cond
     (and (= (month value-date) 12) (= (month entry-date) 1)) inc
     (and (= (month value-date) 1) (= (month entry-date) 12)) dec
     :else identity)))


(def mi-matcher
  (u/regex-elements
   [{:id :value-date
     :regex "[0-9]{6}|null"
     :transform u/parse-date}
    {:id :entry-date
     :regex "[0-9]{4}"
     :transform (comp u/parse-date #(str "0001" %))}
    {:id :amount
     :regex "[CD][0-9]+,[0-9]{0,2}"
     :transform u/parse-amount}
    {;; :id :type
     :regex "N..."}
    {:id :reference
     :regex "(NONREF|.*(\\n.*)?)"}]

   (fn [{:keys [value-date entry-date] :as mutation-info}]
     (if value-date
       (assoc mutation-info
              :entry-date (entry-date-with-year value-date entry-date))
       mutation-info))))

(defn parse-transaction-mutation-info [dialect mt940-mutation-info]
  (cond-> (u/get-matches mi-matcher mt940-mutation-info)
    (= (:id dialect) :asn)
    ((fn [mi]
       (let [[payee-id payee-name] (str/split (:reference mi) #"\n" 2)]
         (cond-> mi
           (not (str/blank? payee-id)) (assoc :payee-id payee-id)
           (not (str/blank? payee-name)) (assoc :payee-name payee-name)))))))
