(ns mt940-edn.balance-info
  (:require
   [mt940-edn.util :as u]))

(def bi-matcher
  (u/regex-elements
   [{:id :debit-credit-flag
     :regex "[CD]"
     :transform {"D" -1 "C" 1}}
    {:id :date
     :regex "[0-9]{6}"
     :transform u/parse-date}
    {:id :currency
     :regex "EUR"}
    {:id :amount
     :regex "[0-9]+,[0-9]{0,2}"
     :transform u/parse-amount}]

   (fn [{:keys [date debit-credit-flag currency amount]}]
     {:date date
      :amount (* debit-credit-flag amount)
      :currency currency})))

(defn parse-statement-balance-info [mt940-balance-info]
  (u/get-matches bi-matcher mt940-balance-info))
