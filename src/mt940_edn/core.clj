(ns mt940-edn.core
  (:gen-class)
  (:require
   [clojure.java.io :as io]
   [clojure.pprint :as pp]
   [mt940-edn.statement :as s]))

(def dialects
  {:knab {:statement-separator "-}"
          :bank-name "KNAB"}
   :asn {:statement-separator "-}{5:}"
         :bank-name "ASN Bank"}
   :abnamro {}})

(defn import-statements [dialect mt940-file]
  (with-open [rdr (io/reader mt940-file)]
    (s/parse-mt940-lines (assoc (get dialects dialect {}) :id dialect)
                         (line-seq rdr))))

(defn -main
  [dialect-name filename]
  (let [dialect (keyword dialect-name)]
    (if-not (dialect dialects)
      (str "Unknown MT940 dialect: " dialect-name)
      (pp/pprint (import-statements dialect filename)))))
